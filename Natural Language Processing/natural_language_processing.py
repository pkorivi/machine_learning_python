import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' Import the tsv file and use the tab as delimiter 
     quoting=3 - ignore quotes '''
dataset = pd.read_csv('Restaurant_Reviews.tsv', delimiter= '\t', quoting = 3)

####### Cleaning the texts #######
import re
''' Stop the words like the, are, etc will be ignored using this library as they are useful to predict the sentiment '''
import nltk
''' Donwloads the stopwords'''
nltk.download('stopwords')
''' Import the stopwords '''
from nltk.corpus import stopwords
''' PorterStemmer simplifies the reviews e.g: 'I loved the food' gets converted to 'I love the food' (simple present) 
     simplifies the matrix by using simple words '''
from nltk.stem.porter import PorterStemmer
''' contains all the different cleaned reviews (punctuations, capitals, etc.) '''
corpus = []
''' For loop for the reviews in this case 1000 reviews '''
for i in range (0, 1000):
    ''' replace anything apart from letters (punctuations) to spaces 
         [^a-zA-Z]', ' ' -  ignore all the letters and anything else replace by space
          apply the regex on the Review column '''
    review = re.sub('[^a-zA-Z]', ' ', dataset['Review'][i])
    ''' transform all the upper case letters into lower case '''
    review = review.lower()
    ''' split the review sentences to words '''
    review = review.split()
    ''' Apply porter stemmer to simplify the language '''
    ps = PorterStemmer()
    ''' If the word is not in the stopwords list, we apply for loop if not we ignore the stopwords in English 
         e.g: ['whole', 'experi', 'underwhelm', 'think', 'go', 'ninja', 'sushi', 'next', 'time'] '''
    all_stopwords = stopwords.words('english')
    ''' 'Not' word was part of the stopwords but we need 'Not' for the reviews as it's important for the sentiment 
          so the following will remove 'not' from the stopwords '''
    all_stopwords.remove('not')
    review = [ps.stem(word) for word in review if not word in set(all_stopwords)]
    ''' Join the words together as a string with a space next to it 
         e.g: whole experi underwhelm think go ninja sushi next time'''
    review = ' '.join(review)
    ''' Add/append all the above outputs to the corpus list '''
    corpus.append(review)

print (corpus)

####### Creating the Bag of Words model #######
''' apply tokenisation using sklearn CountVectorizer '''
from sklearn.feature_extraction.text import CountVectorizer
''' we still have the words which are not helpful to understand the review  e.g: select, Steve, holiday, etc. 
     Use the following code to find out how many words were found from the reviews. Once you have the number of words, 
      then move to the next section '''
#cv = CountVectorizer()
#X = cv.fit_transform(corpus).toarray()
#y = dataset.iloc[:, -1].values
#print (X)
''' Print the number of elements in the first row of the X which indicates the number of words processed '''
#print (len(X[0]))

''' We got 1566 words from the review which includes non frequently used words like select, Steve, holiday, etc. 
     so we remove the words which are not frequently used by selecting only the 1500 most frequently words used in 
      the list '''
cv = CountVectorizer(max_features = 1500)
X = cv.fit_transform(corpus).toarray()
y = dataset.iloc[:, -1].values

####### Splitting the dataset into the Training set and Test set #######
''' Use the existing tool kit to split the data into training and test dataset '''
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)

####### Training the Naive Bayes model on the Training set #######
''' Refer to the Gaussian Bayes model classification model '''
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(X_train, y_train)

####### Predicting the Test set results #######
''' Refer to the Gaussian Bayes model classification model '''
y_pred = classifier.predict(X_test)
print(np.concatenate((y_pred.reshape(len(y_pred),1), y_test.reshape(len(y_test),1)),1))

####### Making the Confusion Matrix #######
''' Refer to the Gaussian Bayes model classification model '''
from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred)
print(cm)
print(accuracy_score(y_test, y_pred))

''' output = [[55 42]
              [12 91]]
    The above is shows: 
     - 55 predictions were correct on class 0 (customers who gave negative reviews)
     - 91 predictions were correct on class 1 (customers who gave positive reviews))
     - 12  predictions were incorrect on class 0 (incorrect predictions of negative reviews)
     - 42  predictions were incorrect on class 1 (incorrect predictions of positive reviews)
'''


''' Output = 0.73
    We have 73% correct predictions '''


