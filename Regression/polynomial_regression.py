import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' In our dataset, the Position field is not required to be considered for dependent variables as the Level field 
     correlates against the Position field which is why we have 1:-1 for X '''
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:-1].values
y = dataset.iloc[:, -1].values

####### Training the Linear Regression model on the whole dataset #######
''' We are using the Linear Regression model to compare the Linear Regression with Polynomial Regression '''
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)

####### Training the Polynomial Regression model on the whole dataset #######
from sklearn.preprocessing import PolynomialFeatures
''' The degree value represents the number x powers. e.g: If degree is configured as 3, the values on the Level column 
     gets converted into a matrix with 3 columns - column 1 - x power 1, column 2 - x power 2, column 3 - x power as 
      required by the polynomial equation below.
     y = b0 + b1 x(1) + b2 x(2) ⋯ bn x(n) - digits in brackets represent the power of x '''
poly_reg = PolynomialFeatures(degree= 6)
X_poly = poly_reg.fit_transform(X)
''' We use the Linear Regression model on the X_poly values calculated above which is why we created a new Linear 
     Regression object called lin_reg2'''
lin_reg2 = LinearRegression()
lin_reg2.fit(X_poly, y)

####### Visualising the Linear Regression results #######
''' Refer to simple_linear_regression.py for details on what the following code does'''
plt.scatter(X, y, color = 'red')
plt.plot(X, lin_reg.predict(X), color = 'blue')
plt.title('Truth or Bluff (Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

###### Visualising the Polynomial Regression results ######
''' Refer to simple_linear_regression.py for details on what the following code does'''
plt.scatter(X, y, color = 'red')
plt.plot(X, lin_reg2.predict(X_poly), color = 'blue')
plt.title('Truth or Bluff (Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

###### Visualising the Polynomial Regression results (for higher resolution and smoother curve) ######
''' This code gives smoother curve but this is not required in real world scenarios as we would have more independent
     variables. The graph is plotted in increments of 0.1 which is what 0.1 below means'''
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, lin_reg2.predict(poly_reg.fit_transform(X_grid)), color = 'blue')
plt.title('Truth or Bluff (Polynomial Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

###### Predicting a new result with Linear Regression ######
''' The predict input should be an array. In python, a single pair of brackets represent a list. With two paris of 
     brackets correspond to rows and columns (first one row and second one column)
    e.g: a = array ([[1, 0], [10, 30]]) equals to [[ 1  0]
                                                   [10 30]]
    6.5 is to predict value of an individual who is in between a Region Manager and Partner. With Linear Regression
     the Salary is around 330K which is not correct as this is more than a Senior Partner earns'''
print(lin_reg.predict([[6.5]]))

###### Predicting a new result with Polynomial Regression ######
''' Let's use Polynomial Linear Regression to calculate the Salary for a 6.5 level individual and this returns as 
     close to 175K which is indeed an accurate prediction. '''
print(lin_reg2.predict(poly_reg.fit_transform([[6.5]])))
