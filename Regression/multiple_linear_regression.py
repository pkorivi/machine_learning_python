import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

####### Encoding the Independent Variable #######
''' In this case the last column (States in column 4) has the characters instead of numerical values which are required 
     to be converted to numerical values. '''
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
'''This method is being applied on column 3 (python number)'''
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [3])], remainder='passthrough')
X = np.array(ct.fit_transform(X))

####### Splitting the dataset into the Training set and Test set #######
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

####### Training the Simple Linear Regression model on the Training set #######
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

####### Predicting the Test set results #######
''' In multiple linear regression, there is no need to feature scale as the co-efficients compensate (why not???)'''
y_pred = regressor.predict(X_test)
'''numpy class to print only 2 values after the decimal to remove the long numbers'''
np.set_printoptions(precision=2)
''' We use concatenate function to concatenate two vectors - they have to be the same size. This is to display
     predicted and test values next to each other. 
    Reshape class converts the default dependent horizontal vector to vertical. The length function calculates the 
      number of rows and places them into one column.
    The final '1' in the argument is to concatenate two vertical vectors (predict and test) into one horizontal matrix
     0 = vertical and 1 = horizontal
    The final output will show how the predicted values are with the test values to give us an idea on how close the
     linear regression adapted'''
print(np.concatenate((y_pred.reshape(len(y_pred),1), y_test.reshape(len(y_test),1)),1))

####### Making a single prediction #######
''' for example the profit of a startup with R&D Spend = 160000, Administration Spend = 130000, 
     Marketing Spend = 300000 and State = 'California' '''
print(regressor.predict([[1, 0, 0, 160000, 130000, 300000]]))

####### Getting the final linear regression equation with the values of the coefficients #######
print(regressor.coef_)
print(regressor.intercept_)
''' Profit=86.6×Dummy State 1−873×Dummy State 2+786×Dummy State 3−0.773×R&D Spend+0.0329×Administration
     +0.0366×Marketing Spend+42467.53'''
