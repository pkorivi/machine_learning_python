import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' In our dataset, the Position field is not required to be considered for dependent variables as the Level field 
     correlates against the Position field which is why we have 1:-1 for X '''
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:-1].values
y = dataset.iloc[:, -1].values
''' covert the y values which are currently in a one dimension array to 2 dimensional array so the Salary can be 
     transformed into one column with multiple columns. len(y) - number of rows and 1 is for number of columns '''
y = y.reshape(len(y),1)

####### Feature Scaling #######
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
''' We have to create two StandardScaler objects for X and y as the mean and standard deviation are different for 
     X and y which is why we have sc_X and sc_y separately'''
X = sc_X.fit_transform(X)
y = sc_y.fit_transform(y)
print(X)
print(y)

###### Training the SVR model on the whole dataset ######
from sklearn.svm import SVR
''' More details, learn about Guassian RPF Kernel'''
regressor = SVR(kernel = 'rbf')
regressor.fit(X,y)

###### Predicting a new result ######
''' The predict method need to be on the same scale as the values as the values of the matrix features the model was 
     trained
    sc.X.transform() will feature scale 6.5 value against the sc.X method earlier
    Now reverse the scaling of y to pre scaled values on Salary column - sc_y.inverse_transform()'''
print ((sc_X.transform([[6.5]])))
print(sc_y.inverse_transform(regressor.predict(sc_X.transform([[6.5]]))))

###### Visualising the SVR results ######
''' In order to scatter plot, we need to inverse the existing X and y values which have been transformed in the previous
     sections using feature scaling. Once the values are inversed, we will have original values of X and y'''
plt.scatter(sc_X.inverse_transform(X), sc_y.inverse_transform(y), color = 'red')
''' The X co-ordinate logic is the same as per above, but for y, we are using regressor.predict function to predict 
     X values and also as this is the case of prediction, we use sc_y rather than sc_X and inverse the values back to 
      original scale (More study on this)'''
plt.plot(sc_X.inverse_transform(X), sc_y.inverse_transform(regressor.predict(X)), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

''' The output is a non linear prediction model and also we  would notice one value sits outside of the curve 
     - support vector which SVR does not catch properly if the values do not fall under the 'tube' (refer notebook)'''

###### Visualising the SVR results (for higher resolution and smoother curve) ######
''' This code gives smoother curve but this is not required in real world scenarios as we would have more independent
     variables. The graph is plotted in increments of 0.1 which is what 0.1 below means'''
X_grid = np.arange(min(sc_X.inverse_transform(X)), max(sc_X.inverse_transform(X)), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(sc_X.inverse_transform(X), sc_y.inverse_transform(y), color = 'red')
plt.plot(X_grid, sc_y.inverse_transform(regressor.predict(sc_X.transform(X_grid))), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()