import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values


####### Splitting the dataset into the Training set and Test set #######
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)


####### Training the Simple Linear Regression model on the Training set #######
''' Run linear regression using the fit method using training date '''
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)


####### Predicting the Test set results #######
''' Predict the results using the test data and the Liner Regression model used above '''
y_pred = regressor.predict(X_test)


####### Visualising the Training set results #######
''' Scatter the training data and plot the regression line
    plot - For the regression line,use x_train on the x-axis and then the predictions of the x_train 
     observations on the y-axis.'''
plt.scatter(X_train, y_train, color = 'red')
plt.plot(X_train, regressor.predict(X_train), color = 'blue')
plt.title('Salary vs Experience (Training set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()


####### Visualising the Test set results #######
''' The regression model calculated earlier against training data will be used against scattered test data which is 
     why we used X_train, regressor.predict(X_train) rather than X_test'''
plt.scatter(X_test, y_test, color = 'red')
plt.plot(X_train, regressor.predict(X_train), color = 'blue')
plt.title('Salary vs Experience (Test set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()


####### Making a single prediction (for example the salary of an employee with 12 years of experience) #######
''' Predict how much salary an employer would earn using the regression model'''
print(regressor.predict([[12]]))


####### Getting the final linear regression equation with the values of the coefficients #######
''' In order to calculate the coefficient and the constant value in the equation y = b(0) + b(1) x X
     b(0) = constant/point that the line crosses the Y intercept
     b(1) = coefficient for the independent variable (X)'''
print(regressor.coef_)
print(regressor.intercept_)

''' Salary=9345.94×YearsExperience+26816.19 '''


