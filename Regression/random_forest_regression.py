''' This regression model is not the best with single data set. This is efficient with multiple data sets but this
     code can be used as a template for multiple data sets '''

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

###### Importing the dataset ######
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:-1].values
y = dataset.iloc[:, -1].values

###### Training the Random Forest Regression model on the whole dataset ######
from sklearn.ensemble import RandomForestRegressor
''' n_estimators is the number of trees and the suggested value is 10 trees. Each tree makes a prediction and the random
     forest regression uses these estimates to make a final estimation '''
regressor = RandomForestRegressor(n_estimators = 10, random_state = 0)
regressor.fit(X, y)
  
###### Predicting a new result ######
print(regressor.predict([[6.5]]))

###### Visualising the Decision Tree Regression results (higher resolution) ######
''' This code gives smoother curve but this is not required in real world scenarios as we would have more independent
     variables. The graph is plotted in increments of 0.1 which is what 0.1 below means'''
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
plt.title('Truth or Bluff (Polynomial Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()