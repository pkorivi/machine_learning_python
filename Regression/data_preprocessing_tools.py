import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


####### Read the CSV file using pandas module #######
dataset = pd.read_csv('Data.csv')
"  iloc, part of pandas pulls data from rows and columns defined and arranges into an array "
"  .values converts pulled iloc pulled values into array "
X = dataset.iloc[:, :-1].values # pull all rows but exclude last column - independent variables only
y = dataset.iloc[:, -1].values  # pull all rows for the last column - dependent variables only


####### Handling the missing data #######
from sklearn.impute import SimpleImputer
SimpleImputer()
""" For pandas’ dataframes with nullable integer dtypes with missing values, missing_values should be set to np.nan "
    numpy is used to find the missing data (nan) "
    'mean' strategy is used to replace the missing values. The mean value is calculated across all the values within 
    that column """
imputer = SimpleImputer(missing_values=np.nan, strategy='mean')
"""  fit module will calculate the values which will be used with transform module. In this case, the values will be 
      mean of the values in the column. inorder to find what the values are (in this case mean value), 
       print(imputer.statistics_) can be used """
imputer.fit(X[:, 1:3])
""" Replace the NaN values on the existing arrays and transform those arrays replacing NaN with mean values using 
     transform module """
X[:, 1:3] = imputer.transform(X[:, 1:3])


####### Encoding categorical data #######
####### 1. Encoding the Independent Variable #######
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
""" The country column on the Data.csv file (France, Germany and Spain) needs to be converted from Labels to binary but
     the labels are converted as 0, 1, 2 for France, Germany and Spain respectively, the Machine Learning model will
      consider the order of these values but in this case, order of the country does not matter.
    So to solve this issue, we will add 3 additional columns (3 countries) and transform countries to binary ( 001, 010,
     100 for France, Germany and Spain)
    Use transformers class to leverage OneHotEncoder class to transform labels to binary. encoder = name, OneHotEncoder 
     = what type of encoder, [0] = make these changes to column 1, passthrough = ignore encoder on remaining columns 
    https://towardsdatascience.com/columntransformer-in-scikit-for-labelencoding-and-onehotencoding-in-machine-learning-c6255952731b """
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [0])], remainder='passthrough')
""" use fit and transform to add the new columns to X and force the output as numpy array (np.array)"""
X = np.array(ct.fit_transform(X))


####### 2. Encoding the Dependent Variable #######
from sklearn.preprocessing import LabelEncoder
""" Using LabelEncoder to convert 'yes' and 'no' values to binary and does not require OneHotEncoder in this case 
     as these don't provide similar purpose as countries """
le = LabelEncoder()
y = le.fit_transform(y)


####### Splitting the dataset into the Training and Test set #######
from sklearn.model_selection import train_test_split
""" split the existing to 80% training and 20% test data in order to validate the accuracy of the learning model 
     with known outputs
    test_size = 0.2 -> defining to use 20% of data set for testing and random_state = 1 -> makes the training and test
     data set to be static so it does not change all the time we execute the code """
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)


####### Feature Scaling #######
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
""" Feature scaling is used to reduce the scale of the numbers to improve learning time/gradient decent to find global 
     minimum
    Standardisation (applied most of the use cases) vs Normalisation (only certain situation) 
    Standardisation: values fall between -3 and +3
     X_stand = X - mean (X) / standard deviation (X)
    Normalisation: values fall between 0 and 1
     X_norm = X - min (X) / max (X) - min (X)      
    Fit method is used to calculate the mean and standard deviation and transform method is used to use the mean and 
     standard deviation to calculate the Standardisation
    In this case, we will not be applying Standardisation to the dummy values (Country names) as the current values 
     provide information about the countries 
    We fit and transform the train data but the test data should only be transformed using the fit sc values from
     training set in order to stop data leaks """

X_train[:, 3:] = sc.fit_transform(X_train[:, 3:])
print (X_train)
X_test[:, 3:] = sc.transform(X_test[:, 3:])
print (X_test)
















