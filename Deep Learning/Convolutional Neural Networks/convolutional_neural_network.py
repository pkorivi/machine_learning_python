#%%

import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator


tf.__version__


#%%
####### Part 1 - Data Preprocessing #######

''' Image trasformation will be applied (e.g: zoom in, rotate the image, horizontal flip, etc.) so the NN will not 
     overfit '''

''' rescale -> feature scaling by diving the pixel values by 255 '''

train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)
####### Preprocessing the Training set #######
''' dataset/training_set -> path for the images 
    target_size -> convert the size so the results would be quicket
    batch_size -> how many batches
    class_mode -> binary or categorical. In this case binary as the output is dog or a cat '''

training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                 target_size = (64, 64),
                                                 batch_size = 32,
                                                 class_mode = 'binary')

#%%
####### Preprocessing the Test set #######
''' Test set images will only be rescaled no other changes will be applied as these are original images used for testing
     similar what we did for test sets in data preprocessing - transform only and shouldn't fit '''
test_datagen = ImageDataGenerator(rescale = 1./255)

test_set = test_datagen.flow_from_directory('dataset/test_set',
                                            target_size = (64, 64),
                                            batch_size = 32,
                                            class_mode = 'binary')

#%%
####### Part 2 - Building the CNN #######
####### Initialising the CNN #######
''' create NN in sequential model - sequence of layers instead of computational graphs '''
cnn = tf.keras.models.Sequential()

####### Step 1 - Convolution #######
''' Filters -> feature detector (refer notes) 32 in first and second convolution layers
    kernel_size -> size of the matrix in this case 3x3 for each feature detector
    activation -> activation function - ReLU
    input_shape = [64, 64, 3] -> we changed the target size to 64x64 in the previous step and 3 represents colour and 
     1 represents Black and White'''
cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu', input_shape=[64, 64, 3]))

#%%
####### Step 2 - Pooling #######
''' Maximum value within a frame (refer notes)
    pool_size -> The size of the frame used to calculate the pooled feature map using the feature map. In this case 2x2 
    strides -> how many strides you want to move to the right. In this case, we stride 2x2 frame '''
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

####### Adding a second convolutional layer #######
''' Same parameters and values from above apart from the input_shape which we only add for the first layer '''
cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu'))
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

#%%
####### Step 3 - Flattening #######
''' converts matrix into an one dimension vector (check notes) '''
cnn.add(tf.keras.layers.Flatten())

#%%
####### Step 4 - Full Connection #######
''' Units -> number of neurons, in this case higher number gives better results
    activation -> ReLU if we have not reached the output layer'''
cnn.add(tf.keras.layers.Dense(units=128, activation='relu'))

#%%
####### Step 5 - Output Layer #######
''' The output dependent value from the spreadsheet takes 0 or 1 value. So we use 1 neuron 
    activation = 'sigmoid' -> Sigmoid function fits better for an output of 1 or 0 '''
cnn.add(tf.keras.layers.Dense(units=1, activation='sigmoid'))

#%%
####### Part 3 - Training the CNN #######
####### Compiling the CNN #######
''' optimizer = 'adam' -> adam optimiser performs Stochastic gradient descent    
   loss = 'binary_crossentropy' -> binary_crossentropy for binary values and other categories use 
    categorical_crossentropy
   metrics = ['accuracy'] -> Used to evaluate the ANN '''
cnn.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

#%%
####### Training the CNN on the Training set and evaluating it on the Test set #######
''' Training and the evaluation of the Test set is conducted on the same step 
    As we are validating in the same step, we use validation_data parameter with our test_set
    epochs = 25 -> number of epochs to improve the accuracy of Neural networks '''
cnn.fit(x = training_set, validation_data = test_set, epochs = 25)

#%%
####### Part 4 - Making a single prediction #######
import numpy as np
from keras.preprocessing import image
''' Load the test image from the directory and change the target size to 64x64 similar to what was performed with the 
     training set and the size of the test image needs to be same as the trained set '''
test_image = image.load_img('dataset/single_prediction/cat_or_dog_2.jpg', target_size = (64, 64))
''' Converts the PIL image format into an array '''
test_image = image.img_to_array(test_image)
''' Training was deployed using batches (batch_size=32) and we are using 1 image rather than 32 for test. So we add 
     dimensions to the batch (read more) '''
test_image = np.expand_dims(test_image, axis = 0)
''' Output of the predict method which would be 0 or 1 using the test_image '''
result = cnn.predict(test_image)
''' The class_indices class from our training_set above will be used to find out if 0 or 1 represents a dog or a cat 
     and viceversa '''
training_set.class_indices
''' Output from above command -> {'cats': 0, 'dogs': 1} '''
''' As we have converted the test image into a batch, we would need to pull the test image from the batch which is
     0 as we only have one batch and 0 for the image as we only have one image to test
    Since we know from class_indices that 1 is a dog and 0 a cat, we get the output '''
if result[0][0] == 1:
    prediction = 'dog'
else:
    prediction = 'cat'

print(prediction)










