#%%
import numpy as np
import pandas as pd
import tensorflow as tf

#%%
''' Check the version of Tensorflow. We are using tensorflow 2'''
print(tf.__version__)


#%%
####### Part 1 - Data Preprocessing #######
####### Importing the dataset #######
dataset = pd.read_csv('Churn_Modelling.csv')
''' We only use the dependent variables so starting from Credit Score '''
X = dataset.iloc[:, 3:-1].values
y = dataset.iloc[:, -1].values

#%%
####### Encoding the Gender Column #######
from sklearn.preprocessing import LabelEncoder
""" Using LabelEncoder to convert 'Male' and 'Female' values to binary and does not require OneHotEncoder in this case
     as these don't provide any purpose """
le = LabelEncoder()
X[:, 2] = le.fit_transform(X[:, 2])
print(X)


#%%
####### Encoding the Country Column #######
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
""" The country column on the Churn_Modelling.csv file (France, Germany and Spain) needs to be converted from Labels to binary but
     the labels are converted as 0, 1, 2 for France, Germany and Spain respectively, the Machine Learning model will
      consider the order of these values but in this case, order of the country does not matter.
    So to solve this issue, we will add 3 additional columns (3 countries) and transform countries to binary ( 001, 010,
     100 for France, Germany and Spain)
    Use transformers class to leverage OneHotEncoder class to transform labels to binary. encoder = name, OneHotEncoder
     = what type of encoder, [1] = make these changes to column 1, passthrough = ignore encoder on remaining columns
    https://towardsdatascience.com/columntransformer-in-scikit-for-labelencoding-and-onehotencoding-in-machine-learning-c6255952731b """
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [1])], remainder='passthrough')
""" use fit and transform to add the new columns to X and force the output as numpy array (np.array)"""
X = np.array(ct.fit_transform(X))
print(X)


#%%
####### Splitting the dataset into the Training set and Test set #######
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

#%%
####### Feature Scaling #######
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
""" Feature scaling is used to reduce the scale of the numbers to improve learning time/gradient decent to find global
     minimum. feature Scaling is essential when it comes to deep learning.
    Standardisation (applied most of the use cases) vs Normalisation (only certain situation)
    Standardisation: values fall between -3 and +3
     X_stand = X - mean (X) / standard deviation (X)
    Normalisation: values fall between 0 and 1
     X_norm = X - min (X) / max (X) - min (X)
    Fit method is used to calculate the mean and standard deviation and transform method is used to use the mean and
     standard deviation to calculate the Standardisation
    In this case, we will not be applying Standardisation to the dummy values (Country names) as the current values
     provide information about the countries
    We fit and transform the train data but the test data should only be transformed using the fit sc values from
     training set in order to stop data leaks """
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#%%
####### Part 2 - Building the ANN #######
####### Initialise the ANN #######
'''  From TF2.0, Keras is now integrated which is why keras class is called from TF2.0 library '''
ann = tf.keras.models.Sequential()

####### Adding the input layer and the first hidden layer #######
''' ann.add(tf.keras.layers.Dense() = add fully connected dense layer
    How many neurons we select does not have a strict rule of thumb. It is based on trial and error 
     units = 6 -> In this case we are using 6 neurons
     activation = 'relu' -> using rectifier activation function '''
ann.add(tf.keras.layers.Dense(units = 6, activation = 'relu'))

####### Adding the second hidden layer #######
''' Same number of neurons can be used in both the first and the second layer '''
ann.add(tf.keras.layers.Dense(units = 6, activation = 'relu'))

####### Adding the output layer #######
''' The output dependent value from the spreadsheet takes 0 or 1 value. So we use 1 neuron 
    activation = 'sigmoid' -> Sigmoid function fits better for an output of 1 or 0 '''
ann.add(tf.keras.layers.Dense(units = 1, activation = 'sigmoid'))

#%%
####### Part 3 - Training the ANN #######
####### Compiling the ANN #######
''' optimizer = 'adam' -> adam optimiser performs Stochastic gradient descent    
   loss = 'binary_crossentropy' -> binary_crossentropy for binary values and other categories use 
    categorical_crossentropy
   metrics = ['accuracy'] -> Used to evaluate the ANN '''
ann.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

####### Training the ANN on the Training set #######
''' batch_size = '32' ->  Batching is preferred over individual feedback for Neural networks
    epochs = 100 -> number of epochs to improve the accuracy of Neural networks '''
ann.fit(X_train, y_train, batch_size = 32, epochs = 100)

''' Once you train the above model, we notice that the accuracy converged to 86% in 20 odd epochs
     Output sample: 
       Epoch 22/100
        250/250 [==============================] - 0s 450us/step - loss: 0.3384 - accuracy: 0.8616 Epoch 23/100 '''
#%%
####### Part 4 - Making the predictions and evaluating the model #######
####### Predicting the result of a single observation #######
''' Use our ANN model to predict if the customer with the following information will leave the bank:
Geography: France
Credit Score: 600
Gender: Male
Age: 40 years old
Tenure: 3 years
Balance: $ 60000
Number of Products: 2
Does this customer have a credit card ? Yes
Is this customer an Active Member: Yes
Estimated Salary: $ 50000
So, should we say goodbye to that customer ? '''

''' Format of the predict method is always a 2D array
    France onehotencoding = 1 0 0
    Enter the values in the order we presented as the input
    As neural networks will always use SC method to scale the values, we use sc.transform method to scale the below 
     values '''
print(ann.predict(sc.transform([[1, 0, 0, 600, 1, 40, 3, 60000, 2, 1, 1, 50000]])))

''' Output = 0.02262884
    The probability of the above customer leaving the bank is 0.022 -> Very low chance to leave the bank '''

''' > 0.5 gives gives True or False output showing if the customer will stay or leave the bank 
print(ann.predict(sc.transform([[1, 0, 0, 600, 1, 40, 3, 60000, 2, 1, 1, 50000]])) > 0.5) '''

''' Important note 1: Notice that the values of the features were all input in a double pair of square brackets. 
     That's because the "predict" method always expects a 2D array as the format of its inputs. And putting our values 
      into a double pair of square brackets makes the input exactly a 2D array.

    Important note 2: Notice also that the "France" country was not input as a string in the last column but as 
     "1, 0, 0" in the first three columns. That's because of course the predict method expects the one-hot-encoded 
       values of the state, and as we see in the first row of the matrix of features X, "France" was encoded as 
        "1, 0, 0". And be careful to include these values in the first three columns, because the dummy variables are 
          always created in the first columns.'''

####### Predicting the Test set results #######
y_pred = ann.predict(X_test)
''' We are converting the values in binary so using y_pred > 0.5 to convert the values into 1 or 0 '''
y_pred = (y_pred > 0.5)
print(np.concatenate((y_pred.reshape(len(y_pred),1), y_test.reshape(len(y_test),1)),1))

####### Making the Confusion Matrix #######
from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred)
print(cm)
accuracy_score(y_test, y_pred)

''' output = [[1524   71]
              [ 197  208]]              
    The above is shows: 
     - 1524 predictions were correct on class 0 (customers who did not buy new SUVs)
     - 208 predictions were correct on class 1 (customer who bought new SUVs)
     - 197  predictions were incorrect on class 0 (customers who did not buy the new SUVs but prediction was they did)
     - 208  predictions were incorrect on class 1 (customer who did buy the new SUVs but prediction was they did not)      
     '''
print(accuracy_score (y_test, y_pred))
''' Output = 0.866
    We have 86% correct predictions '''
