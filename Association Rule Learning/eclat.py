''' Eclat model uses Support and does not consider confidence and lift '''

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' header = none specifies that there are no headers/column names so it will take the values in row 1 '''
dataset = pd.read_csv('Market_Basket_Optimisation.csv', header = None)
''' List transactions as this is required by the apriori module'''
transactions = []
for i in range(0, 7501):
  ''' Create a list of elemets using 7500 rows and 20 columns within the rows. Amend these values to transactions 
       list. The empty columns will be replaced by nan
      i represents rows and j represents columns
      All the values need to be strings for Apriori model which is why we used str function '''
  transactions.append([str(dataset.values[i,j]) for j in range(0, 20)])


####### Training the Apriori model on the dataset #######
from apyori import apriori
''' transactions = transactions -> dataset 
    min_support = 0.003 -> e.g - number of times A and B products appeared together divided by the total number of 
     transactions. Consider the products which appear at least 3 times a day (0.003) 
     (3*7)/7500 - 3 times per day and we have the transanctions for a week and total number of transactions for the week
      is 7500 
    min_confidence = 0.2 -> No rule of thumb trial and error
    min_lift = 3 -> with experience value is 3
    min_length and max_length -> We are offering buy one get one free business model so we the length needs to match
     so we have two products, we got the value as 2 for min_length and max_length '''
rules = apriori(transactions = transactions, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2,
                max_length  = 2 )

####### Visualising the results #######
####### Displaying the first results coming directly from the output of the apriori function #######
results = list(rules)
print(results)
''' Output e.g: [RelationRecord(items=frozenset({'chicken', 'light cream'}), support=0.004532728969470737, 
     ordered_statistics=[OrderedStatistic(items_base=frozenset({'light cream'}), items_add=frozenset({'chicken'}), 
      confidence=0.29059829059829057, lift=4.84395061728395)])
    This shows that if people buy light cream (item_base), there is a good chance that people would buy chicken 
     (items_add) 
    support = 0.0045 more than 3 times a day 
    confidence = 0.29 '''

####### Putting the results well organised into a Pandas DataFrame #######
''' The code came from stackoverflow and this is required to be built from scratch '''
def inspect(results):
    ''' tuple(result[2][0][0])[0] e.g: [RelationRecord(items=frozenset({'chicken', 'light cream'}),
    support=0.004532728969470737, ordered_statistics=[OrderedStatistic(items_base=frozenset({'light cream'}),
    items_add=frozenset({'chicken'}), confidence=0.29059829059829057, lift=4.84395061728395)])
         From the above output, take element [2] -> ordered_statistics=[OrderedStatistic(items_base=frozenset({'light
          cream'}), items_add=frozenset({'chicken'}), confidence=0.29059829059829057, lift=4.84395061728395)]
         From the above output, take element [0] -> OrderedStatistic(items_base=frozenset({'light cream'}),
          items_add=frozenset({'chicken'}), confidence=0.29059829059829057, lift=4.84395061728395)
         From the above output, take element [0] -> items_base=frozenset({'light cream'})
         From the above output, take element [0] -> light cream
        Same logic for the rest of the sections and then place these values under columns
       Removing confidence and lifts as Eclat does not support as opposed to apriori model
    '''
    lhs         = [tuple(result[2][0][0])[0] for result in results]
    rhs         = [tuple(result[2][0][1])[0] for result in results]
    supports    = [result[1] for result in results]
    #confidences = [result[2][0][2] for result in results]
    #lifts       = [result[2][0][3] for result in results]
    return list(zip(lhs, rhs, supports))
resultsinDataFrame = pd.DataFrame(inspect(results), columns = ['Left Hand Side', 'Right Hand Side', 'Support'])
print (resultsinDataFrame)

''' Sample output: 
         Left Hand Side      Right Hand Side   Support
0        light cream         chicken           0.004533 '''

####### Displaying the results sorted by descending lifts #######
''' This method is used to sort the values in descending Support values 
    n = 10 -> number of rows you want to display
    columns = Support -> sort the Support header column values in descending order'''
print(resultsinDataFrame.nlargest(n = 10, columns = 'Support'))
''' sample output: 
          Left Hand Side    Right Hand Side   Support
4         herb & pepper     ground beef       0.015998 
     There is a good chance that someone buying herb and pepper will buy ground beef and this has the highest 
      Support value'''