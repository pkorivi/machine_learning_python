''' We do not know what we are trying to predict but trying to get a pattern with the data. But after running this
     algorithm, we would know what the dependent variable is '''

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' More spending score, the more a customer would spend
    We do not need the CustomerID column as this is not necessary and also we are only using Annual Income and Spending
     Score columns for this method in order to visualise in 2-D'''
dataset = pd.read_csv('Mall_Customers.csv')
X = dataset.iloc[:, [3, 4]].values
''' There is no Y array as we do not have independent variable in this method.
    There is no need to split data for training and test set as at it this point, we do not know what the independent
     variables are '''

####### Using the elbow method to find the optimal number of clusters #######
from sklearn.cluster import KMeans
''' We run the for loop with 1 to 10 number of clusters which is why we are not creating an instance for KMeans 
     class as we did for other models 
     WCSS refer to notes sum of squares of the distances between centroid and data sets '''
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
    ''' 'fit' method to train data '''
    kmeans.fit(X)
    ''' append the values to wcss array and the attribute is inertia which gives the wcss value per each loop '''
    wcss.append(kmeans.inertia_)
''' Use plot function to plot the values of wcss and we will find the number of clsters required based on the graph 
     in this case it is concluded to have 5 clusters '''
plt.plot(range(1, 11), wcss)
plt.title('The Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()

####### Training the K-Means model on the dataset #######
''' Use the number of clusters required concluded from the above step and run the kmeans model again '''
kmeans = KMeans(n_clusters = 5, init = 'k-means++', random_state = 42)
''' Find the dependent variable using fit_predict method. This provides details of 5 clusters'''
y_kmeans = kmeans.fit_predict(X)
print(y_kmeans)
''' Output [3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3 0 3
 0 3 0 3 0 3 1 3 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1 2 4 2 1 2 4 2 4 2 1 2 4 2 4 2 4 2 4 2 1 2 4 2 4 2
 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2 4
 2 4 2 4 2 4 2 4 2 4 2 4 2 4 2] 
  As per the above output, customer 1 belongs to cluster 4 (python number 3) and the last customer belongs to cluster 3
   '''

####### Visualising the clusters #######
''' X[y_kmeans == 0, 0] => y_kmeans == 0 represents customers who are in cluster 0 and these customers are the rows
                        => ,0  represents the first column in this case Annual Income. This becomes X co-ordinate 
    X[y_kmeans == 0, 1] => y_kmeans == 0 represents customers who are in cluster 0 and these customers are the rows
                        => ,1  represents the second column in this case Spending Score. This becomes X co-ordinate
    's' represents size                    '''
plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
''' cluster_centers_ provides the co-ordinates of the clusteroids which can be mapped on the graph '''
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Centroids')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()