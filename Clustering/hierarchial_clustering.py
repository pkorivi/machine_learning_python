''' We do not know what we are trying to predict but trying to get a pattern with the data. But after running this
     algorithm, we would know what the dependent variable is '''

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

####### Importing the dataset #######
''' More spending score, the more a customer would spend
    We do not need the CustomerID column as this is not necessary and also we are only using Annual Income and Spending
     Score columns for this method in order to visualise in 2-D'''
dataset = pd.read_csv('Mall_Customers.csv')
X = dataset.iloc[:, [3, 4]].values
''' There is no Y array as we do not have independent variable in this method.
    There is no need to split data for training and test set as at it this point, we do not know what the independent
     variables are '''

####### Using the dendrogram to find the optimal number of clusters #######
''' The module in this case is imported from scipy rather than sklearn, another popular library '''
import scipy.cluster.hierarchy as sch
''' ward is the best method to minimise the variance '''
dendrogram = sch.dendrogram(sch.linkage(X, method = 'ward'))
plt.title('Dendrogram')
''' The Customers will be X label not Annual Salary or Spending Score'''
plt.xlabel('Customers')
plt.ylabel('Euclidean distances')
plt.show()
''' From the plot, find the largest vertical distance from top to bottom. Start from the first horizontal bar down to  
     the next horizontal bar and continue this process. The largest vertical distance between the horizontal bars 
      becomes the line which confirms the number of clusters. In this case, the amount of clusters is equal to 5 '''

####### Training the Hierarchical Clustering model on the dataset #######
from sklearn.cluster import AgglomerativeClustering
''' n_cluster = 5 from dendrogram, affinity = euclidean distance between the points and linkage = ward method'''
hc = AgglomerativeClustering(n_clusters = 5, affinity = 'euclidean', linkage = 'ward')
y_hc = hc.fit_predict(X)
print (y_hc)
''' Output = [4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4
 3 4 3 4 3 4 1 4 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1 2 1 2 1 2 0 2 0 2 1 2 0 2 0 2 0 2 0 2 1 2 0 2 1 2
 0 2 0 2 0 2 0 2 0 2 0 2 1 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2 0
 2 0 2 0 2 0 2 0 2 0 2 0 2 0 2] 
   As per the above output, customer 1 belongs to cluster 5 (python number 3) and the last customer belongs to cluster 3
   '''

####### Visualising the clusters #######
plt.scatter(X[y_hc == 0, 0], X[y_hc == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
plt.scatter(X[y_hc == 1, 0], X[y_hc == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_hc == 2, 0], X[y_hc == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
plt.scatter(X[y_hc == 3, 0], X[y_hc == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
plt.scatter(X[y_hc == 4, 0], X[y_hc == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.legend()
plt.show()