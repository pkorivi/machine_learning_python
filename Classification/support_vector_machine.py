''' This model chose SVM with Linear model which is why the accuracy score is less than K nearest neighbour '''
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

####### Importing the dataset #######
dataset = pd.read_csv('Social_Network_Ads.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

####### Splitting the dataset into the Training set and Test set #######
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)


####### Feature Scaling #######
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train= sc.fit_transform(X_train)
print (X_train)
X_test= sc.transform(X_test)
print (X_test)

###### Training the Logistic Regression model on the Training set ######
from sklearn.svm import SVC
''' Using linear SVC kernel - linear '''
classifier = SVC(kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train)

###### Predicting a new result ######
''' Transform method is used to scale as per the trained data where we used Feature Scaling'''
print(classifier.predict(sc.transform([[30, 87000]])))

##### Predicting the Test set results #####
''' Predict values based on X_test values and in the next step compare with y_test values to see how efficient the 
     model is'''
y_pred = classifier.predict(X_test)
''' We use concatenate function to concatenate two vectors - they have to be the same size. This is to display
     predicted and test values next to each other. 
    Reshape class converts the default dependent horizontal vector to vertical. The length function calculates the 
      number of rows and places them into one column.
    The final '1' in the argument is to concatenate two vertical vectors (predict and test) into one horizontal matrix
     0 = vertical and 1 = horizontal
    The final output will show how the predicted values are with the test values to give us an idea on how close the
     linear regression adapted'''
print(np.concatenate((y_pred.reshape(len(y_pred),1), y_test.reshape(len(y_test),1)),1))

##### Making the Confusion Matrix #####
from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred)
print(cm)
''' output = [[66  2]
              [ 8 24]]               
    The above is shows: 
     - 66 predictions were correct on class 0 (customers who did not buy new SUVs)
     - 24 predictions were correct on class 1 (customer who bought new SUVs)
     - 8  predictions were incorrect on class 0 (customers who did not buy the new SUVs but prediction was they did)
     - 2  predictions were incorrect on class 1 (customer who did buy the new SUVs but prediction was they did not)      
     '''
print(accuracy_score (y_test, y_pred))
''' Output = 0.90
    We have 90% correct predictions '''

##### Visualising the Training set results #####
''' The code is advanced and this is not required to be understand in real life scenarios'''
from matplotlib.colors import ListedColormap
X_set, y_set = sc.inverse_transform(X_train), y_train
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 10, stop = X_set[:, 0].max() + 10, step = 0.25),
                     np.arange(start = X_set[:, 1].min() - 1000, stop = X_set[:, 1].max() + 1000, step = 0.25))
plt.contourf(X1, X2, classifier.predict(sc.transform(np.array([X1.ravel(), X2.ravel()]).T)).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1], c = ListedColormap(('red', 'green'))(i), label = j)
plt.title('Logistic Regression (Training set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()

##### Visualising the Test set results #####
from matplotlib.colors import ListedColormap
X_set, y_set = sc.inverse_transform(X_test), y_test
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 10, stop = X_set[:, 0].max() + 10, step = 0.25),
                     np.arange(start = X_set[:, 1].min() - 1000, stop = X_set[:, 1].max() + 1000, step = 0.25))
plt.contourf(X1, X2, classifier.predict(sc.transform(np.array([X1.ravel(), X2.ravel()]).T)).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1], c = ListedColormap(('red', 'green'))(i), label = j)
plt.title('Logistic Regression (Test set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()