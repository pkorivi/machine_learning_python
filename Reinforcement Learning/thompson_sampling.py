import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random

####### Importing the dataset #######
dataset = pd.read_csv('Ads_CTR_Optimisation.csv')

####### Implementing UCB #######
''' N = total number of users = Total number of rounds
    d = total number of advertisements
    ads_selected = full list of ads selected, this gets bigger with rounds but will be an empty list to start with
    numbers_of_selections = Ni(n) - number of times the ad i was selected up to round n - [0] * d (initialise list as 
    10 times 0). Ecah time the ad gets selected, the number increments
    sums_of_rewards = Ri(n) - sum of rewards of the ad i up to round n - beginning of the round, the sum of each ads 
    will be 0 which is why we have a list of 10 0's to start with
    total_reward = accumulated over rounds 0 to 10000'''
N = 1000
d = 10
''' Create full list of ads selected. This will be an empty list to start with and gets bigger after iterations '''
ads_selected = []
''' The number of times the ad i got reward 1 up to round n'''
numbers_of_rewards_1 = [0] * d
''' The number of times the ad i got reward 0 up to round n'''
numbers_of_rewards_0 = [0] * d
''' Total rewards will be 0 to start with and increments based on the sums of rewards '''
total_reward = 0

for n in range(0, N):
    ''' Start from ad number 1 (columns) and move across to other ads (1-10)'''
    ad = 0
    ''' max_random will be used to compare with the second for loop '''
    max_random = 0
    for i in range(0, d):
        ''' Step 2 use the random library to calculate the betavariable '''
        random_beta = random.betavariate(numbers_of_rewards_1[i] + 1, numbers_of_rewards_0[i] + 1)
        if (random_beta > max_random):
            ''' update the max_random value every time random_beta is higher than max_random'''
            max_random = random_beta
            ''' This selects the ad which has the max_random value'''
            ad = i
        ''' append the ad that was selected above to the ads_selected list '''
        ads_selected.append(ad)
        ''' Every time an ad was selected, the reward is calculated using the n and the ad values from the dataset'''
        reward = dataset.values[n, ad]
        if reward == 1:
            ''' Every time an ad was with reward = 1 is selected, the numbers_of_rewards_1 list increment by 1 
                 for the respective ad'''
            numbers_of_rewards_1[ad] = numbers_of_rewards_1[ad] + 1
        else:
            ''' Every time an ad was with reward = 0 is selected, the numbers_of_rewards_0 list increment by 1 
                 for the respective ad'''
            numbers_of_rewards_0[ad] = numbers_of_rewards_0[ad] + 1
        ''' total_reward shows the accumulated reward'''
        total_reward = total_reward + reward

####### Visualising the results #######
''' Create a histogram using the stats collected using ads_selected list to see how many times an ad was selected
    ads_selected is a list of 10000 elements where each element is the ad which was selected at a particular round  
     (nth element)'''
plt.hist(ads_selected)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of times each ad was selected')
plt.show()

''' Notes - Thompson Sampling predicted the correct ad even with 500 iterations. This proves that Thompson Sampling is 
             better than UCB'''